import React from 'react';
import {StyleSheet, View, Image} from 'react-native';

export default function Splash() {
  return (
    <View style={styles.container}>
      <Image source={require('./assets/Logo.png')} style={styles.logo} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#490A3D',
  },
  logo: {
    position: 'absolute',
    width: 271,
    height: 80,
  },
});
