import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function About() {
  return (
    <View style={styles.container}>
      <View style={styles.profile}>
        <Text style={styles.titlePage}>Tentang Saya</Text>
        <Image source={require('./assets/0.jpg')} style={styles.profileImg} />
        <Text style={styles.profileName}>Lugas Septiawan</Text>
      </View>
      <View style={styles.socmed}>
        <View style={styles.socmedItem}>
          <Icon name="facebook" size={25} color="#239DF0" />
          <Text style={styles.socmedItemText}>@lugassawan</Text>
        </View>
        <View style={styles.socmedItem}>
          <Icon name="twitter" size={25} color="#128CE5" />
          <Text style={styles.socmedItemText}>@lugassawan</Text>
        </View>
        <View style={styles.socmedItem}>
          <Icon name="instagram" size={25} color="#F64F51" />
          <Text style={styles.socmedItemText}>@lugassawan</Text>
        </View>
      </View>
      <View style={styles.portofolio}>
        <Text style={styles.portofolioDesc}>
          Ingin lebih tahu mengenai saya? Kunjungi tautan dibawah ini.
        </Text>
        <TouchableOpacity>
          <Text style={styles.portofolioLink}>
            https://github.com/lugassawan
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  profile: {
    marginTop: 25,
    alignItems: 'center',
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    textAlign: 'center',
  },
  titlePage: {
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 25,
    color: '#1F1F1F',
  },
  profileImg: {
    marginVertical: 30,
    width: 150,
    height: 150,
    borderRadius: 150 / 2,
  },
  profileName: {
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 19,
    color: '#333333',
  },
  socmed: {
    marginTop: 20,
    marginHorizontal: 20,
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 14,
    lineHeight: 19,
    color: '#333333',
  },
  socmedItem: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  socmedItemText: {
    marginLeft: 10,
  },
  portofolio: {
    marginTop: 30,
    marginBottom: 40,
    marginHorizontal: 20,
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontSize: 14,
    lineHeight: 19,
  },
  portofolioDesc: {
    marginBottom: 10,
    fontWeight: 'normal',
    color: '#333333',
  },
  portofolioLink: {
    fontWeight: 'bold',
    color: '#BD1550',
  },
});
