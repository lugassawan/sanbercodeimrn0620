import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

export default function OnBoarding() {
  return (
    <View style={styles.container}>
      <ScrollView style={styles.scrollView}>
        <View style={styles.illustration}>
          <Text style={styles.greeting}>Selamat Datang</Text>
          <Image
            source={require('./assets/hero_image.png')}
            style={styles.hero}
          />
        </View>
        <View style={styles.button}>
          <TouchableOpacity style={styles.loginBtn}>
            <Text style={styles.loginText}>Masuk</Text>
          </TouchableOpacity>
          <Text style={styles.separator}>atau</Text>
          <TouchableOpacity style={styles.registerBtn}>
            <Text style={styles.registerText}>Daftar</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.aboutBox}>
          <Text style={styles.about}>About Me</Text>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  scrollView: {
    marginHorizontal: 20,
  },
  illustration: {
    marginTop: 80,
  },
  greeting: {
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 33,
    textAlign: 'center',
    color: '#1F1F1F',
  },
  hero: {
    marginTop: 70,
    width: 320,
    height: 224,
  },
  button: {
    marginTop: 50,
  },
  loginBtn: {
    justifyContent: 'center',
    width: 320,
    height: 46,
    backgroundColor: '#490A3D',
    borderRadius: 30,
  },
  loginText: {
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
    color: '#FFFFFF',
  },
  registerBtn: {
    justifyContent: 'center',
    width: 320,
    height: 46,
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#490A3D',
    borderRadius: 30,
  },
  registerText: {
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
    color: '#490A3D',
  },
  separator: {
    marginTop: 10,
    marginBottom: 10,
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
    color: '#333333',
  },
  aboutBox: {
    marginTop: 60,
    marginBottom: 20,
  },
  about: {
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
    color: '#BD1550',
  },
});
