import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,
  ScrollView,
} from 'react-native';

export default class Register extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <View style={styles.logo}>
            <Image
              style={styles.logoImg}
              source={require('./assets/Logo.png')}
            />
          </View>
          <View style={styles.form}>
            <View style={styles.formGroup}>
              <Text style={styles.formLabel}>Nama Lengkap</Text>
              <TextInput
                style={styles.formControl}
                autoCompleteType="name"
                placeholder="Contoh: Jhon Doe"
              />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.formLabel}>Email</Text>
              <TextInput
                style={styles.formControl}
                autoCompleteType="email"
                placeholder="Contoh: example@example.com"
              />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.formLabel}>Password</Text>
              <TextInput
                style={styles.formControl}
                autoCompleteType="password"
                placeholder="Min. 8 karakter"
                secureTextEntry={true}
              />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.formLabel}>Konfirmasi Password</Text>
              <TextInput
                style={styles.formControl}
                autoCompleteType="password"
                placeholder="Min. 8 karakter"
                secureTextEntry={true}
              />
            </View>
          </View>
          <View style={styles.action}>
            <TouchableOpacity style={styles.loginBtn}>
              <Text style={styles.loginText}>Daftar</Text>
            </TouchableOpacity>
            <View style={styles.register}>
              <Text style={styles.registerText}>Sudah ada akun? </Text>
              <TouchableOpacity>
                <Text style={styles.registerActive}>Masuk</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  scrollView: {
    marginHorizontal: 20,
  },
  logo: {
    marginTop: 70,
    alignItems: 'center',
  },
  logoImg: {
    width: 135,
    height: 40,
  },
  form: {
    marginTop: 50,
  },
  formGroup: {
    marginBottom: 20,
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontSize: 14,
    lineHeight: 19,
  },
  formLabel: {
    fontWeight: '600',
    color: '#1F1F1F',
  },
  formControl: {
    width: 320,
    height: 56,
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: '#707070',
    paddingLeft: 10,
    justifyContent: 'center',
    fontWeight: '600',
    color: '#333333',
  },
  forgotPass: {
    fontWeight: '600',
    textAlign: 'right',
    color: '#BD1550',
  },
  action: {
    marginTop: 30,
    marginBottom: 50,
    alignItems: 'center',
  },
  loginBtn: {
    justifyContent: 'center',
    width: 320,
    height: 46,
    backgroundColor: '#490A3D',
    borderRadius: 30,
  },
  loginText: {
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
    color: '#FFFFFF',
  },
  register: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 40,
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  registerText: {
    color: '#1F1F1F',
    fontWeight: 'normal',
  },
  registerActive: {
    color: '#490A3D',
    fontWeight: 'bold',
  },
});
