import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {ProgressBar} from '@react-native-community/progress-bar-android';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class VideoItem extends React.Component {
  render() {
    let skill = this.props.skill;

    return (
      <TouchableOpacity>
        <View style={styles.container}>
          <View style={styles.body}>
            <View style={styles.icon}>
              <Icon name={skill.iconName} size={50} color="#333333" />
            </View>
            <View style={styles.info}>
              <View style={styles.infoSkill}>
                <Text style={styles.skillTitle}>{skill.skillName}</Text>
                <Text style={styles.skillDesc}>{skill.categoryName}</Text>
              </View>
              <View style={styles.infoProgress}>
                <ProgressBar
                  style={styles.bar}
                  styleAttr="Horizontal"
                  color="#BD1550"
                  indeterminate={false}
                  progress={nFormatter(skill.percentageProgress, '%')}
                />
                <Text style={styles.progressText}>
                  {skill.percentageProgress}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

function nFormatter(num, unit) {
  let number = num.replace(unit, '');
  return Number((Number(number) / 100).toFixed(1));
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  body: {
    marginHorizontal: 10,
    marginVertical: 20,
    flexDirection: 'row',
  },
  icon: {
    width: 80,
    height: 80,
    borderRadius: 80 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
    backgroundColor: '#FFFFFF',
  },
  info: {
    marginLeft: 10,
  },
  infoSkill: {
    marginBottom: 10,
  },
  skillTitle: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 21,
    color: '#333333',
  },
  skillDesc: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 12,
    lineHeight: 18,
    color: '#333333',
  },
  infoProgress: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  bar: {
    width: 150,
  },
  progressText: {
    marginLeft: 10,
  },
});
