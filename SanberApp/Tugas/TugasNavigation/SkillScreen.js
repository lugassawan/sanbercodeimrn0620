import React from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import Header from './components/Header';
import SkillItem from './components/SkillItem';

import skillData from './skillData.json';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={skillData.items}
          renderItem={({item}) => <SkillItem skill={item} />}
          keyExtractor={(skill) => skill.id.toString()}
          ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
          ListHeaderComponent={() => <Header />}
          ListFooterComponent={() => <View style={styles.footer} />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  itemSeparator: {
    height: 20,
  },
  footer: {
    marginBottom: 50,
  },
});
