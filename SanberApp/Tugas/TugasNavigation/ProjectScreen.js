import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

export default function ProjectScreen() {
  return (
    <View style={styles.container}>
      <Text>Halaman Proyek</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
});
