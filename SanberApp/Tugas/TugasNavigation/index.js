import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Skill from './SkillScreen';
import Project from './ProjectScreen';
import Add from './AddScreen';
import About from './AboutScreen';
import Login from './LoginScreen';

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const RootStack = createStackNavigator();

const TabScreen = () => (
  <Tab.Navigator
    screenOptions={({route}) => ({
      tabBarIcon: ({focused, color, size}) => {
        let iconName = focused ? 'home' : 'home-outline';

        if (route.name === 'Project') {
          iconName = focused ? 'briefcase' : 'briefcase-outline';
        } else if (route.name === 'Add') {
          iconName = focused ? 'plus-circle' : 'plus-circle-outline';
        }

        return <Icon name={iconName} size={size} color={color} />;
      },
    })}>
    <Tab.Screen name="Skill" component={Skill} />
    <Tab.Screen name="Project" component={Project} />
    <Tab.Screen name="Add" component={Add} />
  </Tab.Navigator>
);

const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Home">
    <Drawer.Screen name="Home" component={TabScreen} />
    <Drawer.Screen name="About" component={About} />
  </Drawer.Navigator>
);

const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen name="Login" component={Login} />
    <RootStack.Screen name="App" component={DrawerScreen} />
  </RootStack.Navigator>
);

export default () => (
  <NavigationContainer>
    <RootStackScreen />
  </NavigationContainer>
);
