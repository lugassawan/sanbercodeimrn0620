import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';

export default function Header() {
  return (
    <View style={styles.sectionOne}>
      <Image style={styles.logo} source={require('./../assets/Logo.png')} />
      <View style={styles.userProfile}>
        <View style={styles.userLeft}>
          <Text style={styles.userName}>Hi, Jhon Doe</Text>
          <Text style={styles.userRole}>Mobile App Developer</Text>
        </View>
        <View style={styles.userRight}>
          <Image
            style={styles.userImg}
            source={require('./../assets/User_male.png')}
          />
        </View>
      </View>
      <View style={styles.totalSkill}>
        <View style={styles.skillGroup}>
          <Text style={styles.skillNumber}>5</Text>
          <Text style={styles.skillCategory}>Bahasa</Text>
        </View>
        <View style={styles.skillGroup}>
          <Text style={styles.skillNumber}>3</Text>
          <Text style={styles.skillCategory}>Framework</Text>
        </View>
        <View style={styles.skillGroup}>
          <Text style={styles.skillNumber}>7</Text>
          <Text style={styles.skillCategory}>Teknologi</Text>
        </View>
      </View>
      <View style={styles.square} />
    </View>
  );
}

const styles = StyleSheet.create({
  sectionOne: {
    marginBottom: 40,
    backgroundColor: '#490A3D',
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  logo: {
    width: 111,
    height: 33,
    marginTop: 25,
    marginHorizontal: 20,
  },
  userProfile: {
    marginTop: 5,
    marginHorizontal: 20,
    flexDirection: 'row',
  },
  userLeft: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  userName: {
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 33,
    color: '#FFFFFF',
  },
  userRole: {
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 14,
    lineHeight: 19,
    color: '#FFFFFF',
  },
  userRight: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  userImg: {
    width: 80,
    height: 80,
  },
  totalSkill: {
    margin: 20,
    flexDirection: 'row',
  },
  skillGroup: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  skillNumber: {
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 36,
    lineHeight: 49,
    textAlign: 'center',
    color: '#FFFFFF',
  },
  skillCategory: {
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 12,
    lineHeight: 16,
    textAlign: 'center',
    color: '#FFFFFF',
  },
  square: {
    marginBottom: 5,
    alignSelf: 'center',
    width: 40,
    height: 2,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
  },
});
