import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

export default function AddScreen() {
  return (
    <View style={styles.container}>
      <Text>Halaman Tambah</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
});
