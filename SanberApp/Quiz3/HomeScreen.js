import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Button,
  Alert,
} from 'react-native';

import products from './data.json';

const DEVICE = Dimensions.get('window');

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      totalPrice: 0,
    };
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  }

  updatePrice(price) {
    //? #Soal Bonus (10 poin)
    //? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    //? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'.
    // Kode di sini
    let {totalPrice} = this.state;
    totalPrice += price;
    this.setState({totalPrice});
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            minHeight: 50,
            width: DEVICE.width * 0.88 + 20,
            marginVertical: 8,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text>
              Hai,{'\n'}
              {/* //? #Soal 1 Tambahan, Simpan userName yang dikirim dari halaman Login pada komponen Text di bawah ini */}
              {this.props.route.params.userName && (
                <Text style={styles.headerText}>
                  {this.props.route.params.userName}
                </Text>
              )}
            </Text>

            {/* //? #Soal Bonus, simpan Total Harga dan state.totalPrice di komponen Text di bawah ini */}
            <Text style={{textAlign: 'right'}}>
              Total Harga{'\n'}
              <Text style={styles.headerText}>{this.state.totalPrice}</Text>
            </Text>
          </View>
          <View></View>
          <TextInput
            style={{backgroundColor: 'white', marginTop: 8}}
            placeholder="Cari barang.."
            onChangeText={(searchText) => this.setState({searchText})}
          />
        </View>

        {/* 
        //? #Soal No 2 (15 poin)
        //? Buatlah 1 komponen FlatList dengan input berasal dari data.json
        //? dan pada prop renderItem menggunakan komponen ListItem -- ada di bawah --
        //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal)

        // Lanjutkan di bawah ini!
        */}
        <FlatList
          data={products.produk}
          renderItem={({item}) => (
            <ListItem
              data={item}
              updatePriceHandler={this.updatePrice.bind(this)}
            />
          )}
          keyExtractor={(item) => item.id}
          numColumns={2}
        />
      </View>
    );
  }
}

class ListItem extends React.Component {
  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  }

  //? #Soal No 3 (15 poin)
  //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device

  render() {
    const data = this.props.data;
    return (
      <TouchableOpacity
        onPress={() => this.props.updatePriceHandler(Number(data.harga))}>
        <View style={styles.itemContainer}>
          <Image
            source={{uri: data.gambaruri}}
            style={styles.itemImage}
            resizeMode="contain"
          />
          <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemName}>
            {data.nama}
          </Text>
          <Text style={styles.itemPrice}>
            {this.currencyFormat(Number(data.harga))}
          </Text>
          <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
          <Button
            onPress={() =>
              Alert.alert('Berhasil', 'Produk ditambah ke keranjang')
            }
            title="BELI"
            color="blue"
          />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    flexDirection: 'column',
    margin: 5,
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  itemImage: {
    width: 120,
    height: 120,
  },
  itemName: {
    marginHorizontal: 10,
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 12,
    height: 50,
  },
  itemPrice: {
    marginHorizontal: 10,
    marginVertical: 5,
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 14,
    color: '#10A5F5',
  },
  itemStock: {
    marginHorizontal: 10,
    marginBottom: 5,
    fontFamily: 'Nunito',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 14,
  },
  itemButton: {},
  buttonText: {},
});
