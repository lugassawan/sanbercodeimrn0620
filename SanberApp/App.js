import React from 'react';
// import YoutubeUI from './Tugas/Tugas12/App';
// import SplashUI from './Tugas/Tugas13/Splash';
// import OnBoardingUI from './Tugas/Tugas13/OnBoarding';
// import LoginUI from './Tugas/Tugas13/Login';
// import RegisterUI from './Tugas/Tugas13/Register';
// import AboutUI from './Tugas/Tugas13/About';
// import TodoApp from './Tugas/Tugas14/App';
// import SkillUI from './Tugas/Tugas14/SkillScreen';
// import ReactNavigation from './Tugas/Tugas15/index';
// import PortoApp from './Tugas/TugasNavigation/index';
import QuizApp from './Quiz3/index';

export default function App() {
  // return <YoutubeUI />;
  // return <SplashUI />;
  // return <OnBoardingUI />;
  // return <LoginUI />;
  // return <RegisterUI />;
  // return <AboutUI />;
  // return <TodoApp />;
  // return <SkillUI />;
  // return <ReactNavigation />;
  // return <PortoApp />;
  return <QuizApp />;
}
