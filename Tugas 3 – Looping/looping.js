/**
 * No. 1 Looping While
 */
console.log("\x1b[36m%s\x1b[0m", "No. 1 Looping While\n");

const START_POSITION = 0;
const FINISH_POSITION = 22;
let isStillLoop = true;
let isLoopForward = true;
let numberPosition = 0;
let loopMessage = "";

while (isStillLoop) {
	if (isLoopForward) {
		if (numberPosition === START_POSITION) {
			loopMessage = "LOOPING PERTAMA";
		} else {
			loopMessage = `${numberPosition} - I love coding`;
		}

		numberPosition += 2;
	} else {
		if (numberPosition === FINISH_POSITION) {
			loopMessage = "\nLOOPING KEDUA";
		} else {
			loopMessage = `${numberPosition} - I will become a mobile developer`;
		}

		numberPosition -= 2;
	}

	console.log(loopMessage);

	if (numberPosition > 20) {
		isLoopForward = false;
	}

	if (numberPosition < 2) {
		isStillLoop = false;
	}
}

/**
 * No. 2 Looping menggunakan for
 */
console.log("\x1b[36m%s\x1b[0m", "\nNo. 2 Looping menggunakan for\n");

let loopMessage2 = "";
for (let i = 1; i <= 20; i++) {
	if (i % 2 === 0) {
		loopMessage2 = `${i} - Berkualitas`;
	} else {
		if (i % 3 === 0) {
			loopMessage2 = `${i} - I Love Coding`;
		} else {
			loopMessage2 = `${i} - Santai`;
		}
	}

	console.log(loopMessage2);
}

/**
 * No. 3 Membuat Persegi Panjang
 */
console.log("\x1b[36m%s\x1b[0m", "\nNo. 3 Membuat Persegi Panjang\n");

let row = 0;
let squareString;

while (row < 4) {
	squareString = "";
	for (let column = 0; column < 8; column++) {
		squareString += "#";
	}

	console.log(squareString);
	row++;
}

/**
 * No. 4 Membuat Tangga
 */
console.log("\x1b[36m%s\x1b[0m", "\nNo. 4 Membuat Tangga\n");

let stair;

for (let row2 = 0; row2 < 7; row2++) {
	stair = "";
	for (let column2 = 0; column2 <= row2; column2++) {
		stair += "#";
	}

	console.log(stair);
}

/**
 * No. 5 Membuat Papan Catur
 */
console.log("\x1b[36m%s\x1b[0m", "\nNo. 5 Membuat Papan Catur\n");

const CHESS_LENGTH = 8;
let chess;
let row3 = 0;
let chessState = -1;

while (row3 < CHESS_LENGTH) {
	chess = "";
	let column3 = 0;

	while (column3 < CHESS_LENGTH) {
		if (chessState === -1) {
			chess += " ";
		} else {
			chess += "#";
		}

		chessState *= -1;
		column3++;
	}

	if (column3 % 2 === 0) {
		chessState *= -1;
	}

	console.log(chess);
	row3++;
}
