/**
 * IF-ELSE
 */
console.log("\x1b[36m%s\x1b[0m", "IF-ELSE\n");

// Start game
function startWerewolfGame(caseId, name, role) {
	console.log(`Case ${caseId}`);
	const roles = ["penyihir", "guard", "werewolf"];

	// Validate input
	if (name.length < 1) {
		console.log("Nama harus diisi!\n");
	} else if (role.length < 1 || !roles.includes(role.toLowerCase())) {
		console.log(`Halo ${name}, Pilih peranmu untuk memulai game!\n`);
	} else {
		console.log(`Selamat datang di Dunia Werewolf, ${name}`);
	}

	// Greet player
	if (role.toLowerCase() === roles[0]) {
		console.log(
			`Halo Penyihir ${name}, kamu dapat melihat siapa yang menjadi werewolf!\n`
		);
	} else if (role.toLowerCase() === roles[1]) {
		console.log(
			`Halo Guard ${name}, kamu akan membantu melindungi temanmu dari serangan werewolf.\n`
		);
	} else if (role.toLowerCase() === roles[2]) {
		console.log(
			`Halo Werewolf ${name}, Kamu akan memakan mangsa setiap malam!\n`
		);
	}
}

// Set name & role
let name1 = "";
let role1 = "";
startWerewolfGame("A", name1, role1);

let name2 = "Jhon";
let role2 = "";
startWerewolfGame("B", name2, role2);

let name3 = "Felix";
let role3 = "Monster";
startWerewolfGame("C", name3, role3);

let name4 = "Smith";
let role4 = "Penyihir";
startWerewolfGame("D", name4, role4);

let name5 = "Rose";
let role5 = "Guard";
startWerewolfGame("E", name5, role5);

let name6 = "Samuel";
let role6 = "Werewolf";
startWerewolfGame("F", name6, role6);

/**
 * SWITCH CASE
 */
console.log("\x1b[36m%s\x1b[0m", "SWITCH CASE\n");

// Convert format date
function convertDate(caseId, day, month, year) {
	if (isNaN(day) || isNaN(month) || isNaN(year)) {
		return console.log(`CASE ${caseId} - Input hanya boleh berupa angka!`);
	}

	if (day < 1 || day > 31) {
		return console.log(`CASE ${caseId} - Masukkan tanggal antara 1 - 31`);
	}

	if (month < 1 || month > 12) {
		return console.log(`CASE ${caseId} - Masukkan bulan antara 1 - 12`);
	}

	if (year < 1) {
		return console.log(`CASE ${caseId} - Masukkan tahun mulai dari 1`);
	}

	let monthName = "";
	switch (parseInt(month)) {
		case 1:
			monthName = "Januari";
			break;
		case 2:
			monthName = "Februari";
			break;
		case 3:
			monthName = "Maret";
			break;
		case 4:
			monthName = "April";
			break;
		case 5:
			monthName = "Mei";
			break;
		case 6:
			monthName = "Juni";
			break;
		case 7:
			monthName = "Juli";
			break;
		case 8:
			monthName = "Agustus";
			break;
		case 9:
			monthName = "September";
			break;
		case 10:
			monthName = "Oktober";
			break;
		case 11:
			monthName = "November";
			break;
		default:
			monthName = "Desember";
	}

	console.log(`CASE ${caseId} - ${day} ${monthName} ${year}`);
}

// Set day, month & year
for (let i = 0; i <= 13; i++) {
	let day = Math.floor(Math.random() * 10) + 1;
	let month = i;
	let year = 2020;

	if (month === 0) {
		month = "Random";
	}

	convertDate(i, day, month, year);
}
