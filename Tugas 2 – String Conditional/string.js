/**
 * Soal No. 1 (Membuat kalimat)
 */
let word = "JavaScript";
let second = "is";
let third = "awesome";
let fourth = "and";
let fifth = "I";
let sixth = "love";
let seventh = "it!";

console.log("\x1b[36m%s\x1b[0m", "Soal No. 1 (Membuat kalimat)\n");
console.log(
	word +
		" " +
		second +
		" " +
		third +
		" " +
		fourth +
		" " +
		fifth +
		" " +
		sixth +
		" " +
		seventh
);
console.log(
	word.concat(
		" ",
		second,
		" ",
		third,
		" ",
		fourth,
		" ",
		fifth,
		" ",
		sixth,
		" ",
		seventh
	)
);
console.log(
	`${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}\n`
);

/**
 * Soal No.2 Mengurai kalimat (Akses karakter dalam string)
 */
let sentence = "I am going to be React Native Developer";

let firstWord = sentence[0];
let secondWord = sentence[2] + sentence[3];
let thirdWord =
	sentence.charAt(5) +
	sentence.charAt(6) +
	sentence.charAt(7) +
	sentence.charAt(8) +
	sentence.charAt(9);
let fourthWord = sentence[11] + sentence[12];
let fifthWord = sentence[14] + sentence[15];
let sixthWord =
	sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
let seventhWord =
	sentence[23] +
	sentence[24] +
	sentence[25] +
	sentence[26] +
	sentence[27] +
	sentence[28];
let eighthWord =
	sentence[30] +
	sentence[31] +
	sentence[32] +
	sentence[33] +
	sentence[34] +
	sentence[35] +
	sentence[36] +
	sentence[37] +
	sentence[38];

console.log(
	"\x1b[36m%s\x1b[0m",
	"Soal No.2 Mengurai kalimat (Akses karakter dalam string)\n"
);
console.log("First Word: " + firstWord);
console.log("Second Word: " + secondWord);
console.log("Third Word: " + thirdWord);
console.log("Fourth Word: " + fourthWord);
console.log("Fifth Word: " + fifthWord);
console.log("Sixth Word: " + sixthWord);
console.log("Seventh Word: " + seventhWord);
console.log("Eighth Word: " + eighthWord + "\n");

/**
 * Soal No. 3 Mengurai Kalimat (Substring)
 */
let sentence2 = "wow JavaScript is so cool";

let exampleFirstWord2 = sentence2.substring(0, 3);
let secondWord2 = sentence2.substring(4, 14);
let thirdWord2 = sentence2.substring(15, 17);
let fourthWord2 = sentence2.substr(18, 2);
let fifthWord2 = sentence2.substr(21, 4);

console.log("\x1b[36m%s\x1b[0m", "Soal No. 3 Mengurai Kalimat (Substring)\n");
console.log("First Word: " + exampleFirstWord2);
console.log("Second Word: " + secondWord2);
console.log("Third Word: " + thirdWord2);
console.log("Fourth Word: " + fourthWord2);
console.log("Fifth Word: " + fifthWord2 + "\n");

/**
 * Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
 */
let sentence3 = "wow JavaScript is so cool";

let exampleFirstWord3 = sentence3.substring(0, 3);
let secondWord3 = sentence3.substring(4, 14);
let thirdWord3 = sentence3.substring(15, 17);
let fourthWord3 = sentence3.substr(18, 2);
let fifthWord3 = sentence3.substr(21, 4);

let firstWordLength = exampleFirstWord3.length;
let secondWordLength = secondWord3.length;
let thirdWordLength = thirdWord3.length;
let fourthWordLength = fourthWord3.length;
let fifthWordLength = fifthWord3.length;

console.log(
	"\x1b[36m%s\x1b[0m",
	"Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String\n"
);

console.log(
	"First Word: " + exampleFirstWord3 + ", with length: " + firstWordLength
);
console.log(
	"Second Word: " + secondWord3 + ", with length: " + secondWordLength
);
console.log("Third Word: " + thirdWord3 + ", with length: " + thirdWordLength);
console.log(
	"Fourth Word: " + fourthWord3 + ", with length: " + fourthWordLength
);
console.log("Fifth Word: " + fifthWord3 + ", with length: " + fifthWordLength);
