/**
 * No. 1
 */
console.log("\x1b[36m%s\x1b[0m", "No. 1\n");

function teriak() {
	return "Halo Sanbers!";
}

console.log(teriak());

/**
 * No. 2
 */
console.log("\x1b[36m%s\x1b[0m", "\nNo. 2\n");

function kalikan(num1, num2) {
	if (isNaN(num1) || isNaN(num2)) {
		return "Input harus berupa angka";
	}

	return num1 * num2;
}

let num1 = 12;
let num2 = 4;

let hasilKali = kalikan(num1, num2);
console.log(hasilKali);

/**
 * No. 3
 */
console.log("\x1b[36m%s\x1b[0m", "\nNo. 3\n");

function introduce(name, age, address, hobby) {
	if (name.length < 1 || address.length < 1 || hobby.length < 1) {
		return "Input tidak boleh kosong";
	}

	if (isNaN(age)) {
		return "Umur harus berupa angka";
	}

	if (age < 1) {
		return "Umur tidak sesuai";
	}

	return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`;
}

let name = "Agus";
let age = 30;
let address = "Jln. Malioboro, Yogyakarta";
let hobby = "Gaming";

let perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
