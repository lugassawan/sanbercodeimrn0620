/**
 * 1. Mengubah fungsi menjadi fungsi arrow
 */
console.log("\x1b[36m%s\x1b[0m", "1. Mengubah fungsi menjadi fungsi arrow\n");

// const golden = function goldenFunction() {
// 	console.log("this is golden!!");
// };

const golden = () => console.log("this is golden!!");

golden();

/**
 * 2. Sederhanakan menjadi Object literal di ES6
 */
console.log(
	"\x1b[36m%s\x1b[0m",
	"\n2. Sederhanakan menjadi Object literal di ES6\n"
);

// const newFunction = function literal(firstName, lastName) {
// 	return {
// 		firstName: firstName,
// 		lastName: lastName,
// 		fullName: function () {
// 			console.log(firstName + " " + lastName);
// 			return;
// 		},
// 	};
// };

const newFunction = (firstName, lastName) => {
	return {
		firstName,
		lastName,
		fullName: () => console.log(`${firstName} ${lastName}`),
	};
};

//Driver Code
newFunction("William", "Imoh").fullName();

/**
 * 3. Destructuring
 */
console.log("\x1b[36m%s\x1b[0m", "\n3. Destructuring\n");

const newObject = {
	firstName: "Harry",
	lastName: "Potter Holt",
	destination: "Hogwarts React Conf",
	occupation: "Deve-wizard Avocado",
	spell: "Vimulus Renderus!!!",
};

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

const { firstName, lastName, destination, occupation } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation);

/**
 * 4. Array Spreading
 */
console.log("\x1b[36m%s\x1b[0m", "\n4. Array Spreading\n");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// const combined = west.concat(east);

const combined = [...west, ...east];

//Driver Code
console.log(combined);

/**
 * 5. Template Literals
 */
console.log("\x1b[36m%s\x1b[0m", "\n5. Template Literals\n");

const planet = "earth";
const view = "glass";
// var before =
// 	"Lorem " +
// 	view +
// 	"dolor sit amet, " +
// 	"consectetur adipiscing elit," +
// 	planet +
// 	"do eiusmod tempor " +
// 	"incididunt ut labore et dolore magna aliqua. Ut enim" +
// 	" ad minim veniam";

let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;

// Driver Code
console.log(before);
