/**
 * Soal No. 1 (Array to Object)
 */
console.log("\x1b[36m%s\x1b[0m", "Soal No. 1 (Array to Object)\n");

function arrayToObject(arr) {
	const ARRAY_LENGTH = arr.length;
	if (ARRAY_LENGTH < 1) {
		return console.log("");
	}

	let now = new Date();
	let thisYear = now.getFullYear();

	for (let i = 0; i < ARRAY_LENGTH; i++) {
		const peopleArr = arr[i];

		let year = Number(peopleArr[3]);
		let age;

		if (isNaN(year)) {
			age = "Invalid Birth Year";
		} else if (year > thisYear || year === 0) {
			age = "Invalid Birth Year";
		} else {
			age = thisYear - year;
		}

		const peopleObj = {
			firstName: peopleArr[0] ? peopleArr[0] : "",
			lastName: peopleArr[1] ? peopleArr[1] : "",
			gender: peopleArr[2] ? peopleArr[2] : "",
			age,
		};

		console.log(
			`${i + 1}. ${peopleObj.firstName} ${peopleObj.lastName} :`,
			peopleObj
		);
	}
}

let people = [
	["Bruce", "Banner", "male", 1975],
	["Natasha", "Romanoff", "female"],
];
arrayToObject(people);

let people2 = [
	["Tony", "Stark", "male", 1981],
	["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);

arrayToObject([]);

/**
 * Soal No. 2 (Shopping Time)
 */
console.log("\x1b[36m%s\x1b[0m", "\nSoal No. 2 (Shopping Time)\n");

function shoppingTime(memberId, money) {
	if (!memberId) {
		return "Mohon maaf, toko X hanya berlaku untuk member saja";
	}

	if (!money || money < 50000) {
		return "Mohon maaf, uang tidak cukup";
	}

	const items = [
		["Sepatu Stacattu", 1500000],
		["Baju Zoro", 500000],
		["Baju H&N", 250000],
		["Sweater Uniklooh", 175000],
		["Casing Handphone", 50000],
	];
	items.sort((a, b) => b[1] - a[1]);

	const shoppingObj = {
		memberId,
		money,
		listPurchased: [],
		changeMoney: money,
	};

	for (let i = 0; i < items.length; i++) {
		let item = items[i];
		if (shoppingObj.changeMoney >= item[1]) {
			shoppingObj.listPurchased.push(item[0]);
			shoppingObj.changeMoney -= item[1];
		}
	}

	return shoppingObj;
}

console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000));
console.log(shoppingTime("234JdhweRxa53", 15000));
console.log(shoppingTime());
console.log(shoppingTime("18kis8WDSAZ", ""));
console.log(shoppingTime("82YSADG2sks"));
console.log(shoppingTime("82YSAD7dadas", 721388));

/**
 * Soal No. 3 (Naik Angkot)
 */
console.log("\x1b[36m%s\x1b[0m", "\nSoal No. 3 (Naik Angkot)\n");

function naikAngkot(passengers) {
	const routes = ["A", "B", "C", "D", "E", "F"];
	const FEE = 2000;
	const output = [];
	const PASSENGER_LENGTH = passengers.length;

	if (PASSENGER_LENGTH > 0) {
		for (let i = 0; i < PASSENGER_LENGTH; i++) {
			const passengerData = passengers[i];

			let passenger = {
				penumpang: passengerData[0] ? passengerData[0] : "",
				naikDari: passengerData[1] ? passengerData[1] : "",
				tujuan: passengerData[2] ? passengerData[2] : "",
				bayar: 0,
			};

			const isPassangerHasName = passenger.penumpang.length > 0;
			const startIndex = routes.findIndex(
				(route) => route === passenger.naikDari.toUpperCase()
			);
			const finishIndex = routes.findIndex(
				(route) => route === passenger.tujuan.toUpperCase()
			);

			if (startIndex > -1 && finishIndex > -1) {
				if (finishIndex > startIndex && isPassangerHasName) {
					let totalRoute = finishIndex - startIndex;
					passenger.bayar = totalRoute * FEE;
				}
			}

			if (!isPassangerHasName || startIndex < 0 || finishIndex < 0) {
				passenger = {};
			}

			output.push(passenger);
		}
	}

	return output;
}

console.log(
	naikAngkot([
		["Dimitri", "B", "F"],
		["Icha", "A", "B"],
	])
);
console.log(naikAngkot([]));
console.log(
	naikAngkot([
		["Dimitri", "G", "H"],
		["Icha", "D", "G"],
		["Acha", "F", "A"],
		["Achi", "C", "C"],
	])
);
console.log(
	naikAngkot([
		["", "A", "F"],
		["Dimitri", "", "F"],
		["Icha", "A", ""],
	])
);
console.log(naikAngkot([[], ["Dimitri"], ["Icha", "A"]]));
