const readBooksPromise = require("./promise.js");

let books = [
	{ name: "LOTR", timeSpent: 3000 },
	{ name: "Fidas", timeSpent: 2000 },
	{ name: "Kalkulus", timeSpent: 4000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise
function start(time, index) {
	if (index >= books.length) {
		return;
	}

	return readBooksPromise(time, books[index])
		.then((timeLeft) => start(timeLeft, index + 1))
		.catch((e) => console.log(e));
}

start(10000, 0);
