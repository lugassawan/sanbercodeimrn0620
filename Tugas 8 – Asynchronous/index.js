const readBooks = require("./callback.js");

let books = [
	{ name: "LOTR", timeSpent: 3000 },
	{ name: "Fidas", timeSpent: 2000 },
	{ name: "Kalkulus", timeSpent: 4000 },
];

// Tulis code untuk memanggil function readBooks di sini
function start(time, index) {
	if (index >= books.length) {
		return;
	}

	return readBooks(time, books[index], function (timeLeft) {
		return start(timeLeft, index + 1);
	});
}

start(10000, 0);
