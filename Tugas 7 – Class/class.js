/**
 * 1. Animal Class
 */
console.log("\x1b[36m%s\x1b[0m", "1. Animal Class\n");

console.log("\x1b[31m%s\x1b[0m", "#Release 0");

class Animal {
	constructor(name, legs = 4, cold_blooded = false) {
		this._name = name;
		this._legs = legs;
		this._cold_blooded = cold_blooded;
	}

	get name() {
		return this._name;
	}

	set name(animalName) {
		this._name = animalName;
	}

	get legs() {
		return this._legs;
	}

	set legs(animallegs) {
		this._legs = animallegs;
	}

	get cold_blooded() {
		return this._cold_blooded;
	}

	set cold_blooded(animalColdBlooded) {
		this._cold_blooded = animalColdBlooded;
	}
}

let sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

console.log("\x1b[31m%s\x1b[0m", "\n#Release 1");

class Ape extends Animal {
	constructor(name) {
		super(name, 2);
	}

	yell() {
		console.log("Auooo");
	}
}

class Frog extends Animal {
	constructor(name) {
		super(name, undefined, true);
	}

	jump() {
		console.log("hop hop");
	}
}

let sungokong = new Ape("kera sakti");
sungokong.yell();
console.log(
	`Nama: ${sungokong.name}, berkaki ${sungokong.legs}, berdarah dingin: ${
		sungokong.cold_blooded ? "Ya" : "Tidak"
	}`
);

let kodok = new Frog("buduk");
kodok.jump();
console.log(
	`Nama: ${kodok.name}, berkaki ${kodok.legs}, berdarah dingin: ${
		kodok.cold_blooded ? "Ya" : "Tidak"
	}`
);

/**
 * 2. Function to Class
 */
console.log("\x1b[36m%s\x1b[0m", "\n2. Function to Class\n");

class Clock {
	constructor({ template }) {
		this.template = template;
	}

	render() {
		let date = new Date();

		let hours = date.getHours();
		if (hours < 10) hours = "0" + hours;

		let mins = date.getMinutes();
		if (mins < 10) mins = "0" + mins;

		let secs = date.getSeconds();
		if (secs < 10) secs = "0" + secs;

		let output = this.template
			.replace("h", hours)
			.replace("m", mins)
			.replace("s", secs);

		console.log(output);
	}

	start() {
		this.render();
		this.timer = setInterval(() => this.render(), 1000);
	}

	stop() {
		clearInterval(this.timer);
	}
}

let clock = new Clock({ template: "h:m:s" });
clock.start();
